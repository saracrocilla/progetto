<?php

    require 'header.php';
?>

    <main>
        <div class="signup-div">
        <header class="user__header">
        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg" alt="" />
        <h1 class="user__title">Registrati gratuitamente</h1>
        </header>
       

        <form class="form" action="../includes/signup.inc.php" method="post">
            <div class="form__g">
                <input class="form-control mr-sm-2" type="text" name="Username" placeholder="Nome">
            </div>
             <div class="form__g">
                <input class="form-control mr-sm-2" type="text" name="Cognome" placeholder="Cognome">
            </div>
            <div class="form__g">
                <input class="form-control mr-sm-2t" type="text" name="E-mail" placeholder="E-mail">
            </div>
            <div class="form__g">
                <input class="form-control mr-sm-2" type="password" name="Password" placeholder="Password">
            </div>
            <div class="form__g">
                <input class="form-control mr-sm-2" type="password" name="pwd-repeat" placeholder="Ripeti password">
            </div>
            <button type="submit" class="btn-signin" value="" name="signup-submit" >Sign up</button>
        </form> 
            
            <?php
            if(isset($_GET["error"])){
                if($_GET["error"] == "emptyfields"){
                    echo '<p class="alert alert-danger" role="alert">Fill in all fields!</p>';
                }else if($_GET["error"] == "invalidmailuid"){
                    echo '<p class="alert alert-danger" role="alert">Invalid username and e-mail!</p>';
                }else if($_GET["error"] == "invalidid"){
                    echo '<p class="alert alert-danger" role="alert">Invalid username!</p>';
                }else if($_GET["error"] == "invalidmail"){
                    echo '<p class="alert alert-danger" role="alert">Invalid e-mail!</p>';
                }else if($_GET["error"] == "passwordcheck"){
                    echo '<p class="alert alert-danger" role="alert">Your password do not match!</p>';
                }else if($_GET["error"] == "ussertaken"){
                    echo '<p class="alert alert-danger" role="alert">Username is already taken!</p>';
                }
            }else if(isset($_GET["signup"])){
                if ($_GET["signup"] == "success"){
                   echo '<p class="alert alert-success" role="alert">Signup successfully!</p>';
                }
            }
        ?>
       </div>
    </main>

