<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		
            <script src="../js/jquery.min.js"></script>
		<link rel="stylesheet" href="../css/bootstrap.css" />
                <link rel="stylesheet" href="../css/bootstrap.min.css" />
                <link rel="stylesheet" href="../css/sty.css" />
                
                <script src="../js/bootstrap.min.js"></script>
              
		<style>
		.popover
		{
		     width: auto;
		    max-width: 800px;
		}
		</style>
	</head>
	<body>
		<div class="container-fluid">
                       <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <a class="navbar-brand" href="index">Cosplan</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                              <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="../SRC/index.php">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tutti" href="../includes/fetch_item.php?categoria=tutti">Tutti i prodotti</a>
                                </li>
                         <div id="navbar-cart" class="navbar-collapse collapse" style="float:left;">
						<ul class="nav navbar-nav">
							<li>
								<a id="popover2" class="nav-link" data-container="body" data-toggle="popover" data-placement="bottom" >
									Categorie
								</a>
							</li>
						</ul>
					</div>
     
  </div>

                   
					 <?php
                        if(isset($_SESSION['ID'])){
                            echo ' <div id="navbar-cart" class="navbar-collapse collapse" style="float:right;">
						<ul class="nav navbar-nav">
							<li>
								<a id="cart-popover" class="btn btn-outline-success my-2 my-sm-0" data-container="body" data-toggle="popover" data-placement="bottom" title="Shopping Cart">
									<span class="glyphicon glyphicon-shopping-cart"></span>
									<span class="badge"></span>
									<span class="total_price">Eur 0.00</span>
								</a>
							</li>
						</ul>
					</div>
                                  <form class="form-inline my-2 my-lg-0" style="float:right;" action="../includes/logout.inc.php" method="post">
                                  <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="logout-submit">Logout</button>
                                  </form>';
                        }else{
                            echo '<form class="form-inline my-2 my-lg-0" style="float:right;" action="../includes/login.inc.php" method="post">
                                  <input class="form-control mr-sm-2" type="text" name="mail" placeholder=" Username/E-mail...">
                                  <input class="form-control mr-sm-2" type="password" name="pwd" placeholder=" Password...">
                                  <button  class="btn btn-outline-success my-2 my-sm-0" type="submit" name="login-submit">Login</button>
                                  </form>   
                   
                                  <a class="btn btn-outline-success my-2 my-sm-0" style="float:right;" href="signup.php">Sign up</a>';
                        }
                    
                    ?>
			
                            </nav>
                    <div id="popover_content_wrapper" style="display: none">
				<span id="cart_details"></span>
				<div align="right">
					<a href="#" class="btn btn-primary" id="check_out_cart">
					<span class="glyphicon glyphicon-shopping-cart"></span> Check out
					</a>
					<a href="#" class="btn btn-default" id="clear_cart">
					<span class="glyphicon glyphicon-trash"></span> Clear
					</a>
				</div>
			</div>
                    
                     <div id="popover_content" style="display: none">
				
				<div align="left">
					<a href="../includes/fetch_item.php?categoria=Anime" style=" width:100px;"class="btn btn-default" id="Anime">
					Anime
					</a>
                                    </div>
                         <div align="left">
					<a href="../includes/fetch_item.php?categoria=Videogiochi" style=" width:100px; margin-top: 3px;" class="btn btn-default" id="Videogiochi">
					Videogiochi
					</a>
				</div>
			</div>
                    <div class="index-div" style="margin-top: 0px">
                        <?php
                            if(isset($_SESSION['ID'])){
                                echo '<p class="alert alert-success" role="alert">You are <a href="#" class="alert-link">logged in!</a></p>';


                            }else{
                                echo '<p class="alert alert-danger" role="alert">You are <a href="#" class="alert-link">logged out!</a></p>';
                            }
                        ?>
                         </div>
		</div>
        </body>
</html>
<script> 

$(document).ready(function(){

	

	load_cart_data();
    
	
        
        function load_cart_data()
	{
		$.ajax({
			url:"../includes/fetch_cart.php",
			method:"POST",
			dataType:"json",
			success:function(data)
			{
				$('#cart_details').html(data.cart_details);
				$('.total_price').text(data.total_price);
				$('.badge').text(data.total_item);
			}
		});
	}
        

                
	$('#cart-popover').popover({
            
		html : true,
        container: 'body',
        content:function(){
        	return $('#popover_content_wrapper').html();
        }
	});
        $('#popover2').popover({
          
		html : true,
        container: 'body',
        content:function(){
        	return $('#popover_content').html();
        }
	});

	$(document).on('click', '.add_to_cart', function(){
		var product_id = $(this).attr("id");
		var product_name = $('#name'+product_id+'').val();
		var product_price = $('#price'+product_id+'').val();
		var product_quantity = $('#quantity'+product_id).val();
		var action = "add";
		if(product_quantity > 0)
		{
			$.ajax({
				url:"../includes/action.php",
				method:"POST",
				data:{product_id:product_id, product_name:product_name, product_price:product_price, product_quantity:product_quantity, action:action,},
				success:function(data)
				{
					load_cart_data();
                                       
					alert("Item has been Added into Cart");
                                  
				}
			});
		}
		else
		{
			alert("lease Enter Number of Quantity");
		}
	});

	$(document).on('click', '.delete', function(){
		var product_id = $(this).attr("id");
		var action = 'remove';
		if(confirm("Are you sure you want to remove this product?"))
		{
			$.ajax({
				url:"../includes/action.php",
				method:"POST",
				data:{product_id:product_id, action:action},
				success:function()
				{
					load_cart_data();
					$('#cart-popover').popover('hide');
					alert("Item has been removed from Cart");
				}
			})
		}
		else
		{
			return false;
		}
	});

	$(document).on('click', '#clear_cart', function(){
		var action = 'empty';
		$.ajax({
			url:"../includes/action.php",
			method:"POST",
			data:{action:action},
			success:function()
			{
				load_cart_data();
				$('#cart-popover').popover('hide');
				alert("Your Cart has been clear");
			}
		});
	});
        
        
   $(document).on('click', '.view-product', function(){
		var product_id = $(this).attr("id");
		
		
			$.ajax({
				url:"fetch_element.php",
				method:"POST",
				data:{product_id: product_id},
				success:function(data)
				{
				
                                window.location.href ="fetch_element.php?product_id="+ product_id;
			}
				});
			});
                       
   
	});

</script> 
