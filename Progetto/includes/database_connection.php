<?php

$servername = "localhost";
$dbUsername = "root";
$dbPassword = "";
$dbName = "cosplays";

$connect = mysqli_connect($servername, $dbUsername, $dbPassword, $dbName);

if (!$connect){
    die("Connection failed:".mysqli_connection_error());
}
